#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Binding Cr@ns-made pour s'interfacer avec la base LDAP

Copyright (C) 2010-2013 Cr@ns <roots@crans.org>
Authors: Antoine Durand-Gasselin <adg@crans.org>
         Nicolas Dandrimont <olasd@crans.org>
         Olivier Iffrig <iffrig@crans.org>
         Valentin Samir <samir@crans.org>
         Daniel Stan <dstan@crans.org>
         Vincent Le Gallic <legallic@crans.org>
         Pierre-Elliott Bécue <becue@crans.org>
"""

from lc_ldap import *
