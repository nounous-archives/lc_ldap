
lc_ldap.attributs -- Conteneurs pour les attributs LDAP
=======================================================

.. automodule:: attributs
   :members:
   :private-members:
   :special-members:
   :show-inheritance:

