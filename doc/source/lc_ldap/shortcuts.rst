
lc_ldap.shortcuts -- Raccourcis de connexion à la base LDAP
===========================================================

.. automodule:: shortcuts
   :members:
   :private-members:
   :special-members:
   :show-inheritance:

