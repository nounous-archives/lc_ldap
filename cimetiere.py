#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import lc_ldap
import filter2 as filter

cimetiere_root = '/home/cimetiere_lc'

def find_in_filter(lf, key):
    if lf:
        if lf[0] == '=':
             if lf[1] == key:
                 return lf[2]
        else:
            for i in lf[1:]:
                ret=find_in_filter(i, key)
                if ret:
                    return ret
    return None

def valide_filter(lf, ldif):
    if lf:
        if lf[0] == '=':
            return True if [i for i in ldif.get(lf[1], [lf[2]]) if lf[2].lower() in i.lower()] else False
        elif lf[0] == '!=':
            return False if [i for i in ldif.get(lf[1], []) if lf[2].lower() in i.lower()] else True
        elif lf[0] == '|':
            return reduce(lambda x,y: x or y, [valide_filter(i, ldif) for i in lf[1:]] + [False, False])
        elif lf[0] == '&':
            return reduce(lambda x,y: x and y, [valide_filter(i, ldif) for i in lf[1:]] + [True, True])
        else:
            raise ValueError("Unknown operator %s" % lf[0])

def condition((fdate, ftime, fdn, path), date=None, aid=None, mid=None, fl=None):
    if date:
        if date[0] != '-' and fdate < date[0]:
            return False
        if date[1] != '-' and fdate > date[1]:
            return False
    if aid:
       if not ("aid=%s," % aid) in fdn:
           return False
    if mid:
       if not ("mid=%s," % mid) in fdn:
           return False
    if fl:
       (dn, ldif)= lc_ldap.lc_ldap.ressuscite_build_ldif(path)
       return valide_filter(fl, ldif)
    return True

    
def find(type, fl=None, date=None):
    """type est un objectclass primaire
       fl un filtre humain du genre key1=val1&key2=val2|key3=val3
       date un tuple (debut, fin) avec - pour \infinity"""
    if not type in os.listdir(cimetiere_root):
        raise ValueError("type %s invalid, valid types are %s"  % (type,os.listdir(cimetiere_root)))
    cimetiere  = map(lambda s:  '%s/%s/%s' % (cimetiere_root, type, s), os.listdir('%s/%s' % (cimetiere_root, type)))
    cimetiere.sort()
    fl = filter.human_to_list(fl)
    aid = find_in_filter(fl, "aid")
    mid = find_in_filter(fl, "mid")
    valid=[]
    for file in cimetiere:
        name=os.path.basename(file)
        try:
            (fdate, time, dn) = name.split('_')
        except ValueError:
            sys.stderr.write("Nom mal formaté ignoré %r\n" % name)
            continue
        if condition((fdate, time, dn, file), fl=fl, aid=aid, mid=mid, date=date):
            valid.append(file)
    return valid
