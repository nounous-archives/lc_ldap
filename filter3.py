#!/usr/bin/env python
"""Plus joli"""

import pyparsing
unicodePrintables = u''.join([
    unichr(c)
    for c in xrange(65536)
    if not unichr(c).isspace()
])

txt = pyparsing.Word(
    "".join([
        c
        for c in unicodePrintables
        f c not in '=()|&><'
    ]),
    exact=1
)

ne = pyparsing.Literal('!=')
eq = pyparsing.Literal('=')
andop = pyparsing.oneOf('&')
orop = pyparsing.oneOf('|')
leq = pyparsing.Literal('<=')
geq = pyparsing.Literal('>=')
lt = pyparsing.Literal('<')
gt = pyparsing.Literal('>')

expr = pyparsing.operatorPrecedence(
    txt,
    [
        (ne, 2, pyparsing.opAssoc.RIGHT),
        (eq, 2, pyparsing.opAssoc.RIGHT),
        (leq, 2, pyparsing.opAssoc.RIGHT),
        (geq, 2, pyparsing.opAssoc.RIGHT),
        (lt, 2, pyparsing.opAssoc.RIGHT),
        (gt, 2, pyparsing.opAssoc.RIGHT),
        (andop, 2, pyparsing.opAssoc.LEFT),
        (orop, 2, pyparsing.opAssoc.LEFT),
    ]
)

def simplify(l):
    if not isinstance(l, list):
        return l
    if len(l) == 1:
        return simplify(l[0])
    else:
        return [simplify(i) for i in l]

def toprefix(l):
    if not isinstance(l, list):
        return l
    op = l[1]
    args = [toprefix(i) for i in l if i!=op]
    return [op] + args

def toldapfilter(l):
    if not isinstance(l, list):
        return l
    op=l[0]
    if op == "=":
        return "%s=%s" % (l[1], l[2])
    elif op == "!=":
        return "!(%s=%s)" % (l[1], l[2])
    elif op == "<=":
        return "%s<=%s" % (l[1], l[2])
    elif op == ">=":
        return "%s>=%s" % (l[1], l[2])
    elif op == "<":
        return "&(%s=*)(!(%s>=%s))" % (l[1], l[1], l[2])
    elif op == ">":
        return "&(%s=*)(!(%s<=%s))" % (l[1], l[1], l[2])
    return op + ''.join(['(%s)' % toldapfilter(i) for i in l[1:]])

def human_to_list(data):
    if data:
        return toprefix(simplify(expr.parseString(data).asList()))

def human_to_ldap(data):
        return "(%s)" % toldapfilter(human_to_list(data))

