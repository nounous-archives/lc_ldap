#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ATTRIBUTS_TESTS.PY-- Tests sur les fonctions de gestion des attributs.
#
# Copyright (C) 2010 Cr@ns <roots@crans.org>
# Author: Nicolas Dandrimont <olasd@crans.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys as _sys
_sys.path.append('/usr/scripts/gestion')
del _sys

import config
import unittest

from lc_ldap_tests import LDAPTest, auto_suite
from attributs import *

class nomTest(LDAPTest):
    """Tests sur l'attribut nom"""
    tests = ["acceptNom"]
    def acceptNom(self):
        """Accepte les noms de la liste définie"""
        self.attrify_light(u"Dandrimont", "nom")
        self.attrify_light(u"D'Andrimont", "nom")
        self.attrify_light(u"Parret-Fréaud", "nom")
        self.attrify_light(u"Space Man", "nom")
        self.assertRaises(Exception, self.attrify_light, "Stringman", "nom")
        self.assertRaises(Exception, self.attrify_light, u" Spaceman", "nom")
        self.assertRaises(Exception, self.attrify_light, u"Spéciaux!?", "nom")

class prenomTest(LDAPTest):
    """Tests sur l'attribut prenom"""
    tests = ["acceptPrenom"]
    def acceptPrenom(self):
        """Accepte les prénoms de la liste définie"""
        self.attrify_light(u"Nicolas", "prenom")
        self.attrify_light(u"Gérard", "prenom")
        self.attrify_light(u"John-Éric", "prenom")
        self.attrify_light(u"Charles Jean", "prenom")
        self.assertRaises(Exception, self.attrify_light, "String", "prenom")
        self.assertRaises(Exception, self.attrify_light, u" Space", "prenom")
        self.assertRaises(Exception, self.attrify_light, u"Spéciaux!?", "prenom")

class telTest(LDAPTest):
    """Tests sur l'attribut tel"""
    tests = ["acceptTel", "normalizeTel"]
    def acceptTel(self):
        """Accepte les numéros de la liste définie"""
        self.attrify_light(u"01 23 45 67 89", "tel")
        self.attrify_light(u"0000000000", "tel")
        self.attrify_light(u"00 33 1 23 45 67 89", "tel")
        self.attrify_light(u"+42 (0) 1 23456789", "tel")
        self.assertRaises(Exception, self.attrify_light, "", "tel")
        self.assertRaises(Exception, self.attrify_light, u"caracteres?!", "tel")

    def normalizeTel(self):
        """Normalise correctement les numéros de téléphone"""
        attr = self.attrify_light(u"01 23 45 67 89", "tel")
        self.assertEqual(str(attr), "0123456789")
        attr = self.attrify_light(u"0000000000", "tel")
        self.assertEqual(str(attr), "0000000000")
        attr = self.attrify_light(u"00 33 1 23 45 67 89", "tel")
        self.assertEqual(str(attr), "0033123456789")
        attr = self.attrify_light(u"+42 (0) 1 23456789", "tel")
        self.assertEqual(str(attr), "0042123456789")

class yearTest(LDAPTest):
    """Tests sur des champs entiers"""
    tests = ["acceptInt"]
    field = None
    def acceptInt(self):
        """Accepte les entiers prédéfinis"""
        self.attrify_light(u"1999", self.field)
        self.attrify_light(u"2008", self.field)
        self.assertRaises(ValueError, self.attrify_light, u"1990", self.field)
        self.assertRaises(ValueError, self.attrify_light, unicode(config.ann_scol()+1), self.field)

class paiementTest(yearTest):
    """Tests sur l'attribut paiement"""
    field = "paiement"

class carteEtudiantTest(yearTest):
    """Tests sur l'attribut paiement"""
    field = "carteEtudiant"

TEST_SUITE = unittest.TestSuite((
        auto_suite(nomTest),
        auto_suite(prenomTest),
        auto_suite(telTest),
        auto_suite(paiementTest),
        auto_suite(carteEtudiantTest),
        ))

if __name__ == "__main__":
    RUNNER = unittest.TextTestRunner()
    RUNNER.run(TEST_SUITE)
