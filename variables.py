#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Définitions de variables utiles pour lc_ldap. """

import os

##: Encodage qu'on utilise pour parler à l'utilisateur si on n'a pas réussi à le détecter
#fallback_encoding = 'utf-8'
#: Encodage de la base LDAP
ldap_encoding = "utf-8"

uri = "ldap://ldap.adm.crans.org/"
if os.getenv('DBG_LDAP', False):
    host = os.getenv('DBG_LDAP', '1')
    if host == '1':
        host = 'localhost'
    uri = 'ldap://%s/' % host

#: dn racine de l'endroit où sont stockées les données
base_dn = "ou=data,dc=crans,dc=org"
#: dn racine de l'endroit où sont stockés les logs
log_dn = "cn=log"
#: dn racine de l'endroit où sont stockés les services à redémarrer
services_dn = "ou=services,dc=crans,dc=org"
#: dn pour se binder en root
admin_dn = "cn=admin,dc=crans,dc=org"
#: dn pour se binder en readonly
readonly_dn = "cn=readonly,dc=crans,dc=org"
#: dn racine de l'endroit où sont stockés les invités (artefact garbage ?)
invite_dn = "ou=invites,ou=data,dc=crans,dc=org"


# Protection contre les typos
#: Droit de créer
created = 'created'
#: Droit de modifier
modified = 'modified'
#: Droit de supprimer
deleted = 'deleted'

#: Mot de passe de la base de tests
ldap_test_password = '75bdb64f32'

