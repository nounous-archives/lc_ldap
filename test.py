#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

# import lib standard
import psycopg2
import traceback
import random
import string
import os
import sys

## import locaux
import lc_ldap
import shortcuts
import variables

from gestion.affich_tools import anim, OK, cprint, ERREUR

mail_format = False
show_traceback = False
if "--traceback" in sys.argv:
    show_traceback = True
fast_test = False
if "--fast" in sys.argv:
    fast_test = True
if "--mail" in sys.argv:
    mail_format = True
    OK = "OK"
    ERREUR = "ERREUR"
def randomMAC():
    mac = [ 0x00, 0x16, 0x3e,
        random.randint(0x00, 0x7f),
        random.randint(0x00, 0xff),
        random.randint(0x00, 0xff) ]
    return u':'.join(map(lambda x: u"%02x" % x, mac))

def randomStr(n=10):
    return ''.join( random.choice(string.lowercase + string.digits) for i in range(n))

adherent_ldif = {
 'prenom' : [u'Totoé'],
 'nom' : [u'passoir'],
 'chbre' : [u'EXT'],
 'tel' : [u'0000000000'],
 'mail' : [u'nobody@test.org'],
 'etudes' : [u'ÉNS', u'1', u'A0'],
}

machine_ldif = {
 'macAddress' : [randomMAC()],
 'host' : [u"autotest-%s.crans.org" % randomStr() ]
}

borne_ldif  = {
 'macAddress' : [randomMAC()],
 'host' : [u"autotest-%s.crans.org" % randomStr() ],
 'canal' : [u"11"],
 'puissance' : [u"52"],
 'hotspot' : [u'FALSE'],
}

club_ldif = {
 'nom' : [ u'autotest-club' ],
 'chbre' : [ u'EXT' ],
}

facture_ldif = {
 'modePaiement' : [u'liquide'],
}


def keys_of_list_of_dict(l, type=''):
    """Récupère la liste de tous les attributs existant.
    Teste l'écriture des objets si on est en mode rw"""
    keys = set()
    if l:
        anime=anim("Test des objets %r" % type, 0 if mail_format else len(l))
        anime.reinit()
    ok=True
    for item in l:
        keys = keys.union(item.attrs.keys())
        try:
            if item.mode in  ['w', 'rw']:
                with item:
                    item.save()
        except Exception as error:
            anime.reinit()
            print ERREUR
            print item
            if show_traceback: print traceback.format_exc()
            else: print "\t%r" % error
            ok = False
        if not mail_format:
            anime.cycle()
    if l:
        anime.reinit()
        print OK if ok else ERREUR
    keys = list(keys)
    keys.sort()
    return keys

def test_list_of_dict(keys, list):
    """Test l'instanciation des attributs"""
    for key in keys:
        anim("\tTest de l'attribut %s" % key)
        ok = True
        for item in list:
            try: item.get(key, [])
            except psycopg2.OperationalError as error:
                print ERREUR
                if show_traceback: print traceback.format_exc()
                else: print "\t%r" % error
                return
            except Exception as error:
                if ok: print ERREUR
                print item
                if show_traceback: print traceback.format_exc()
                else: print "\t%r" % error
                ok = False
        if ok: print OK

def tests_machines(parent_dn, realm_list, ipsec=False):
    """Test la création et la suppression de machines dans la realm_list"""
    for realm in realm_list:
        anim("Creation de machines %s" % realm)
        try: 
            if realm == 'bornes':
                mldif = borne_ldif
            else:
                mldif = machine_ldif
            with conn.newMachine(parent_dn, realm,  mldif) as machine:
                if ipsec: machine['ipsec'] = u'auto'
                machine.create()
        except Exception as error:
            print ERREUR
            if show_traceback: print traceback.format_exc()
            else: print "\t%r" % error
        else:
            print OK
            anim("Suppression d'une machines %s" % realm)
            try:
                with conn.search(u'mid=%s' % machine['mid'][0], mode='rw')[0] as machine:
                    machine.delete()
                    del(machine)
            except Exception as error:
                print ERREUR
                if show_traceback: print traceback.format_exc()
                else: print "\t%r" % error
            else: print OK

if not mail_format:
    print "Test de la librairie lc_ldap"

    print "Connexion"
conn = shortcuts.lc_ldap_test()

if not mail_format:
    print u"Tests effectués avec les droits %s " % ', '.join(conn.droits)

###############################################
# Instanciation de tous les attributs de tous #
# les adhérents et de toutes les machines     #
###############################################

anim("Appel de allMachinesAdherents en %s" % ('ro' if fast_test else 'rw'))
try:
    if fast_test:
        machines, adherents = conn.allMachinesAdherents()
    else:
        machines, adherents = conn.allMachinesAdherents(mode='rw')
except EnvironmentError as error:
    print ERREUR
    if show_traceback: print traceback.format_exc()
    else: print "\t%s" % error
    exit(1)
except Exception as error:
    print ERREUR
    if show_traceback: print traceback.format_exc()
    else: print "\t%r" % error
    anim("Fallback en ro")
    machines, adherents = conn.allMachinesAdherents()
    print OK
else:
    print OK

machines_attrs_keys = keys_of_list_of_dict(machines, 'machines')
if not mail_format:
    print "Test des attributs des machines"
test_list_of_dict(machines_attrs_keys, machines)

adherents_attrs_keys = keys_of_list_of_dict(adherents, 'adherents')
if not mail_format:
    print "Test des attributs des adhérents"
test_list_of_dict(adherents_attrs_keys, adherents)

factures = conn.search(u"fid=*", sizelimit=-1, mode='ro' if fast_test else 'rw')
factures_attrs_keys = keys_of_list_of_dict(factures, 'factures')
if not mail_format:
    print "Test des attributs des factures"
test_list_of_dict(factures_attrs_keys, factures)

certificats = conn.search(u"xid=*", sizelimit=-1, mode='ro' if fast_test else 'rw')
certificats_attrs_keys = keys_of_list_of_dict(certificats, 'certificats')
if not mail_format:
    print "Test des attributs des certificats"
test_list_of_dict(certificats_attrs_keys, certificats)

if not mail_format:
    print "Test de création d'objets"
    print "Instanciation"


##############################################
# Création  d'un Adhérent et de ses machines #
##############################################

anim("Creation d'un adherent")
try:
    with conn.newAdherent(adherent_ldif) as adherent:
        adherent.create()
except Exception:
    print ERREUR
    print traceback.format_exc()
    adherent = None
else:
    print OK

    tests_machines(adherent.dn, ["adherents", "adherents-v6", "personnel-ens"])
    tests_machines(adherent.dn, ["wifi-adh", "wifi-adh-v6"], ipsec=True)


#############################################
# Création et suppression de machines Crans #
#############################################

tests_machines(variables.base_dn, ["adm", "serveurs", "serveurs-v6", "adm-v6"])
tests_machines(variables.base_dn, ["bornes"])


######################
# Création d'un club #
######################

anim("Creation d'un club")
try:
    with conn.newClub(club_ldif) as club:
        club['responsable'] = unicode(adherent['aid'][0])
        club.create()
except Exception:
    print ERREUR
    print traceback.format_exc()
else:
    print OK

    tests_machines(club.dn, ["adherents"])

    anim("Suppression d'un club")
    try:
        with conn.search(u'cid=%s' % club['cid'][0], mode='rw')[0] as club:
            club.delete()
    except Exception:
        print ERREUR
        print traceback.format_exc()
    else: print OK


#######################################
# Création et suppression de factures #
#######################################

if adherent:
    anim("Creation d'une facture")
    try:
        with conn.newFacture(adherent.dn, facture_ldif) as facture:
            facture.create()
    except Exception as error:
        print ERREUR
        if show_traceback: print traceback.format_exc()
        else: print "\t%r" % error
    else:
        print OK
        anim("Suppression d'une facture")
        try:
            with conn.search(u'fid=%s' % facture['fid'][0], mode='rw')[0] as facture:
                facture.delete()
        except Exception:
            print ERREUR
            print traceback.format_exc()
        else: print OK


############################
# Supression d'un adhérent #
############################

if adherent:
    anim("Suppression d'un adherent")
    try:
        with conn.search(u'aid=%s' % adherent['aid'][0], mode='rw')[0] as adherent:
            adherent.delete()
    except Exception:
        print ERREUR
        print traceback.format_exc()
    else: print OK
