#!/usr/bin/env python
# -*- encoding: utf-8 -*-


""" Affiche en human readable un ldif (de slapcat, par exemple)
    en décodant les parties en base64 """

import re
import base64
import sys

regexp = re.compile("(?P<cle>[A-Za-z]*):: (?P<valeur>(?:[A-Za-z0-9/+]*(?:\n )?)*={0,3})")

def remplace(objet):
    groups = objet.groupdict()
    return "%s: %s" % (groups["cle"], base64.b64decode(groups["valeur"]))


def perform(texte):
    return re.sub(regexp, remplace, texte)

if __name__ == "__main__":
    filename = sys.argv[1]
    with open(filename) as f:
        texte = f.read()
    with open(filename + ".b64decoded", "w") as g:
        g.write(perform(texte).replace("\n ", ""))
    print "Done :)"
